FROM nginx
RUN mkdir /cert/
COPY default.conf /etc/nginx/conf.d/
COPY ./www /var/www
