set -e;
apt-get update -y;
apt-get install certbot -y;
certbot certonly --standalone -d matthewcane.co.uk --non-interactive --agree-tos -m cert@matthewcane.co.uk;
