<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115641393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-115641393-1');
	</script>

	<title>Matthew Cane | CV</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="content/images/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="content/images/favicon-16x16.png" sizes="16x16" />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	<script src="https://kit.fontawesome.com/6f02663e38.js" crossorigin="anonymous"></script>
	<style>
		html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
		@media print{
			div.break {break-inside: avoid;}
			footer {display: none;}
			#page {
				background-color: white !important;
			}
		}
	</style>

	<?php
		// Age Calc
		$birthday = date_create("1993/11/1");
		$now = date_create();
		$age = date_diff($birthday, $now)->format('%Y');
	 ?>

</head>
<body id="page" class="w3-light-grey">

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">

	<!-- The Grid -->
	<div class="w3-row-padding">

		<!-- Left Column -->
		<div class="w3-third">

			<div class="w3-white w3-text-grey w3-card-4">
				<div class="w3-display-container">
					<img src="content/images/face.jpg" alt="My Face" style="width:100%;">
					<div class="w3-display-bottomleft w3-container w3-text-white">
						<h2 class="w3-xxlarge" style="text-shadow:1px 1px 0 #444">Matthew Cane</h2>
					</div>
				</div>
				<div class="w3-container">
					<p class="w3-xlarge"><b><i class="far fa-smile fa-fw w3-margin-right w3-text-teal"></i>About Me</b></p>
					<p><i class="fas fa-birthday-cake fa-fw w3-margin-right w3-large w3-text-teal"></i><?php echo $age?> Years Old</p>
					<p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Computing Student</p>
					<p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>Bristol, UK</p>
					<p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i><a href="mailto:contact@matthewcane.co.uk">contact@matthewcane.co.uk</a></p>
					<p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>07450 297700</p>

					<hr>

					<p class="w3-xlarge"><b><i class="fas fa-list-ul fa-fw w3-margin-right w3-text-teal"></i>Skills</b></p>
					<p><i class="fas fa-comments fa-fw w3-margin-right w3-large w3-text-teal"></i>Excellent Interpersonal Skills</p>
					<p><i class="fab fa-python fa-fw w3-margin-right w3-large w3-text-teal"></i>Python 3</p>
					<p><i class="fab fa-html5 fa-fw w3-margin-right w3-large w3-text-teal"></i>HTML 5</p>
					<p><i class="fab fa-css3-alt fa-fw w3-margin-right w3-large w3-text-teal"></i>CSS 3</p>
					<p><i class="fab fa-js-square fa-fw w3-margin-right w3-large w3-text-teal"></i>Javascipt</p>
					<p><i class="fas fa-copyright fa-fw w3-margin-right w3-large w3-text-teal"></i>C/C++ Programing</p>
					<p><i class="fas fa-database fa-fw w3-margin-right w3-large w3-text-teal"></i>Database Design, SQL & MongoDB</p>
					<p><i class="fas fa-exchange-alt fa-fw w3-margin-right w3-large w3-text-teal"></i>Computer Networking</p>
					<p><i class="fab fa-linux fa-fw w3-margin-right w3-large w3-text-teal"></i>Linux & Command Line Operation</p>
					<p><i class="fas fa-code-branch fa-fw w3-margin-right w3-large w3-text-teal"></i>Git Source Control</p>
					<p><i class="fab fa-docker fa-fw w3-margin-right w3-large w3-text-teal"></i>Docker</p>

					<hr>

					<p class="w3-xlarge w3-text-theme"><b><i class="fa fa-globe fa-fw w3-margin-right w3-text-teal"></i>Languages</b></p>
					<p><i class="fas fa-comment fa-fw w3-margin-right w3-large w3-text-teal"></i>English</p>

					<hr>

					<p class="w3-xlarge w3-text-theme"><b><i class="fas fa-folder fa-fw w3-margin-right w3-text-teal"></i>Projects</b></p>

					<div class="w3-container">
						<i class="fab fa-gitlab fa-fw w3-margin-right w3-large w3-text-teal"></i>
						<a href="https://gitlab.com/Matthew.Cane" class="w3-button w3-teal w3-round">See my projects on Gitlab</a>
					</div>

					<hr>

					<p class="w3-xlarge w3-text-theme"><b><i class="fa fa-download fa-fw w3-margin-right w3-text-teal"></i>Download CV</b></p>
					<div class="w3-container">
						<i class="fas fa-file-pdf fa-fw w3-margin-right w3-large w3-text-teal"></i>
						<a href="https://glcdn.githack.com/Matthew.Cane/curriculum-vitae/-/raw/master/CV-2-Page.pdf" download class="w3-button w3-teal w3-round">Dowload CV in PDF format</a>
					</div>

					<hr>
				</div>
			</div><br>

		<!-- End Left Column -->
		</div>

		<!-- Right Column -->
		<div class="w3-twothird">

			<div class="w3-container w3-card w3-white w3-margin-bottom break">
				<h2 class="w3-text-grey w3-padding-16"><i class="fas fa-user fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Personal Statement</h2>
				<div class="w3-container">
					<p>
						<?php
					 		include(__DIR__.'/content/text/personalStatement.txt')
						?>
					</p>
					<br>

				</div>
			</div>

			<div class="w3-container w3-card w3-white w3-margin-bottom break">
				<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-graduation-cap fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Education</h2>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>BSc(Hons) in Computing | University of the West of England</b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>September 2018 - July 2022 <span class="w3-tag w3-teal w3-round">Current</span></h6>
					<p>
						<?php include(__DIR__.'/content/text/education/UWE.txt')?>
					</p>
					<hr>
				</div>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>Access to HE in Computing | City College Plymouth</b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>September 2017 - May 2018</h6>
					<p>
						<?php include(__DIR__.'/content/text/education/cityCollege.txt')?>
					</p>
					<hr>
				</div>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>Sport (Outdoor and Adventure) | Dutchy College </b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>September 2010 - June 2012</h6>
					<p>
						<?php include(__DIR__.'/content/text/education/dutchy.txt')?>
					</p>
					<hr>
				</div>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>General Secondary Education | Saltash.net Community School</b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>September 2005 - June 2010</h6>
					<p>
						<?php include(__DIR__.'/content/text/education/saltash.txt')?>
					</p><br>
				</div>
			</div>

			<div class="w3-container w3-card w3-white w3-margin-bottom break">
				<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-building fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Work Experience</h2>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>University of the West of England | Espresso Programming Tutor</b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>November 2020 - Present</h6>
					<p>
						<?php include(__DIR__.'/content/text/workExperience/espressoProgramming.txt')?>
					</p>
					<hr>
				</div>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>Morrisons | Night Assistant</b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>June 2019 - September 2019</h6>
					<p>
						<?php include(__DIR__.'/content/text/workExperience/morrisons.txt')?>
					</p>
					<hr>
				</div>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>Tesco | Dotcom Picker</b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>July 2015 - August 2018</h6>
					<p>
						<?php include(__DIR__.'/content/text/workExperience/tesco.txt')?>
					</p>
					<hr>
				</div>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>Mountain Warehouse | General Sales Assistant</b></h5>
					<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2009 - 2013</h6>
					<p>
						<?php include(__DIR__.'/content/text/workExperience/mountainWarehouse.txt')?>
					</p>
				</div>
			</div>

			<div class="w3-container w3-card w3-white w3-margin-bottom break">
				<h2 class="w3-text-grey w3-padding-16"><i class="fas fa-gamepad fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Other Experiences and Interests</h2>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>Scouting and Charitable Work</b></h5>
					<p>
						<?php include(__DIR__.'/content/text/interests/scouting.txt')?>
					</p>
					<hr>
				</div>
				<div class="w3-container">
					<h5 class="w3-opacity"><b>PC Gaming</b></h5>
					<p>
						<?php include(__DIR__.'/content/text/interests/gaming.txt')?>
					</p>
				</div>
			</div>


			<div class="w3-container w3-card w3-white break">
				<h2 class="w3-text-grey w3-padding-16"><i class="fas fa-thumbs-up fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>References</h2>

				<div class="w3-container w3-card w3-white w3-half w3-margin-right"> <!--Left Container-->
					<h5 class="w3-opacity"><b>Dr Christopher Ford | Lecturer in Computing at City College Plymouth</b></h5>
					<h6 class="w3-opacity"><b><!--cford@cityplym.ac.uk--><!--Contact Details Here--></b></h6>
					<p class="w3-xlarge w3-serif w3-opacity"><i class="fas fa-quote-left fa-fw w3-margin-right w3-xxxlarge w3-text-teal w3-opacity-off"></i>
					<?php include(__DIR__.'/content/text/references/christopherFord.txt')?>
					<br><br></p>
				</div>

				<div class="w3-container w3-card w3-white w3-rest"> <!--Right Container-->
					<h5 class="w3-opacity"><b>Emily Shepherd | Development Lead at Candide Gardening <!-- Best Person Ever --></b></h5>
					<h6 class="w3-opacity"><b><!--Contact Details Here--></b></h6>
					<p class="w3-xlarge w3-serif w3-opacity"><i class="fas fa-quote-left fa-fw w3-margin-right w3-xxxlarge w3-text-teal"></i>
					<?php include(__DIR__.'/content/text/references/emilyShepherd.txt')?>
					</p>
				</div>

			<br>
			</div>

    <!-- End Right Column -->
    </div>

  <!-- End Grid -->
</div>

  <!-- End Page Container -->
</div>

<footer class="w3-container w3-teal w3-center w3-margin-top">
	<p class="w3-large">Find me on:</p>
	<!--<a href="https://www.facebook.com/matt.cane"><p class="fab fa-facebook w3-hover-opacity w3-xxlarge"></p></a>-->
	<a href="https://www.instagram.com/matthew_cane"><p class="fab fa-instagram w3-hover-opacity w3-xxlarge w3-margin-left w3-margin-right"></p></a>
	<a href="https://www.linkedin.com/in/matthew-cane1993"><p class="fab fa-linkedin w3-hover-opacity w3-xxlarge w3-margin-right"></p></a>
	<a href="https://gitlab.com/Matthew.Cane"><p class="fab fa-gitlab w3-hover-opacity w3-xxlarge w3-margin-right"></p></a>
	<a href="https://github.com/MatthewCane"><p class="fab fa-github-square w3-hover-opacity w3-xxlarge w3-margin-right"></p></a>
	<a href="https://stackoverflow.com/users/11212318/matthew-cane"><p class="fab fa-stack-overflow w3-hover-opacity w3-xxlarge"></p></a>
	<hr>
</footer>

</body>
</html>
