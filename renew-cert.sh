set -e;
docker-compose stop;
rm /cert/*
certbot renew;
cp -LR /etc/letsencrypt/live/matthewcane.co.uk /cert/;
docker-compose start;
